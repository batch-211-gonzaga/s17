/*
  1. Create a function which is able to prompt the user to provide their fullname, age, and location.
    -use prompt() and store the returned value into a function scoped variable within the function.
    -show an alert to thank the user for their input
    -display the user's inputs in messages in the console.
    -invoke the function to display your information in the console.
    -follow the naming conventions for functions.
*/
  
  //first function here:
  function askUserInfo() {
    const fullName = prompt("What is your name?");
    const age = prompt("How old are you?");
    const location_ = prompt("Where do you live?");
    alert("Thank you for your input!");

    console.log("Hello, "+ fullName);
    console.log("You are " + age + " years old.");
    console.log("You live in " + location_);
  }

  askUserInfo();

/*
  2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
    -invoke the function to display your information in the console.
    -follow the naming conventions for functions.
  
*/

  //second function here:
  function logBands() {
    console.log("1. Soundgarden");
    console.log("2. Alice in Chains");
    console.log("3. Pearl Jam");
    console.log("4. Tool");
    console.log("5. Queens of the Stone Age");
  }

  logBands();

/*
  3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
    -Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
    -invoke the function to display your information in the console.
    -follow the naming conventions for functions.
  
*/
  
  //third function here:
  function logMovies() {
    console.log("1. The Godfather");
    console.log("Rotten Tomatoes Rating: 97%");
    console.log("2. The Godfather, Part II");
    console.log("Rotten Tomatoes Rating: 96%");
    console.log("3. Monty Python and the Holy Grail");
    console.log("Rotten Tomatoes Rating: 98%");
    console.log("4. Shaun of the Dead");
    console.log("Rotten Tomatoes Rating: 93%");
    console.log("5. Hot Fuzz");
    console.log("Rotten Tomatoes Rating: 91%");
  }

  logMovies();

/*
  4. Debugging Practice - Debug the following codes and functions to avoid errors.
    -check the variable names
    -check the variable scope
    -check function invocation/declaration
    -comment out unusable codes.
*/

printFriends();

function printFriends() {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  console.log("You are friends with:")
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
}

//console.log(friend1);
//console.log(friend2);
